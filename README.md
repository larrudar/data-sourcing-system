The Data Sourcing System (DSS) is part of an international collaboration, led by Fermilab, which proposed the AM+FPGA approach as the CMS Outer Tracker L1 Trigger.

The SPRACE group from Brazil is the main developer of DSS vhdl code and IPbus python scripts for the User Interface. 